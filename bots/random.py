import random

# TODO expand list https://dragonball.fandom.com/wiki/Category:Characters
#  https://neoencyclopedia.fandom.com/wiki/List_of_Dragon_Ball_characters
#  https://en.wikipedia.org/wiki/List_of_Dragon_Ball_characters
import string

REGENTS = (
    ('Son Goku', 'M',),
    ('Bulma', 'Z',),
    ('Yamcha', 'M',),
    ('Krillin', 'M',),
    ('Tien Shinhan', 'M',),
    ('King Piccolo', 'M',),
    ('Son Gohan', 'M',),
    ('Vegeta', 'M',),
    ('Son Goten', 'M',),
    ('Trunks', 'M',),
    ('Frieza', 'M',),
    ('Cell', 'M',),
    ('Majin Buu', 'M',),
    ('Android #18', 'Z',),
    ('Chiaotzu', 'M',),
    ('Chi Chi', 'Z',),
    ('Mr. Satan', 'M',),
    ('Master Roshi', 'M',),
    ('Pan', 'M',),
    ('Great Demon King Piccolo', 'M',),
    ('Videl', 'Z',),
    ('Yajirobe', 'M',),
)

CLASSES = (
    (1, 'M', 'M', 'Mág',),
    (2, 'M', 'B', 'Alchymista',),
    (3, 'B', 'B', 'Válečník',),
    (4, 'B', 'M', 'Klerik',),
    (5, 'Z', 'B', 'Hraničář',),
    (6, 'Z', 'M', 'Druid',),
    (7, 'C', 'M', 'Nekromant',),
    (8, 'C', 'B', 'Theurg',),
    (9, 'S', 'M', 'Iluzionista',),
    (10, 'S', 'B', 'Barbar',),
    (11, 'F', 'M', 'Vědma',),
    (12, 'F', 'B', 'Amazonka',),
)

QUOTES = (
    '“Are you ready now to witness a power not seen for thousands of years?” – Vegeta',
    '“The ocean is so salty because everyone pees in it.” – Goku',
    '“If I win, promise you won’t destroy the earth.” – Goku',
    '“KA-ME-HA-ME-HAAAAAAAAAAAAAA.” – Goku',
    '“Nobody preaching about justice is ever a good guy.” – Beerus',
    '“Before creation… must come destruction!” – Beerus',
    '“Anyone stronger than the god of destruction does not exist.” – Beerus',
    '“There’s no such thing as fair or unfair in battle. There is only victory or in your case, defeat.” – Vegeta',
    '“Trunks, Bulma, I’m doing this for you. And yes, even for you, Kakarot.” – Vegeta',
    '“Are you ready now to witness a power not seen for thousands of years?” – Vegeta',
    '“Push through the pain, giving up hurts more.” – Vegeta',
    '“No, I can’t lose!” – Vegeta',
    '“You can take control of my mind and my body, but there is one thing a Saiyan always keep… his PRIDE!” – Vegeta',
    '“There are three things I cannot tolerate: cowardice, bad haircuts, and military insurrection. It is very unfortun'
    'ate that our friend Vegeta possesses all three of these.” – Frieza',
)


def rand_regent_sex() -> (str, str):
    choice = random.choice(REGENTS)
    return '%s %03d' % (choice[0], rand_int(100)), choice[1]


def rand_class() -> (int, str, str, str):
    return random.choice(CLASSES)


def rand_quote() -> (int, str, str, str):
    return random.choice(QUOTES)


def rand_alnum(length: int = 10) -> str:
    return ''.join(random.choice(string.ascii_letters + string.digits) for i in range(length))


def rand_email() -> str:
    return '%s@seznam.cz' % rand_alnum(10)


def rand_chance(chance: float) -> bool:
    return random.random() < chance


def rand_float(a: float, b: float) -> float:
    return a + random.random() * (b - a)


def rand_int(a: int, b: int = None) -> int:
    """
    a is inclusive, b is exclusive.

    if b is none, is from 0 to a exclusive.

    identical to random.randrange(a, b), just wrapper for consistency
    """
    return random.randrange(a, b)


def rand_num(length: int) -> int:
    return int(''.join(random.choice(string.digits) for i in range(length)))
