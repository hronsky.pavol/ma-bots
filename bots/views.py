from django.shortcuts import redirect, get_object_or_404, render
from django.views.decorators.http import require_POST
from django.views.generic import ListView

from bots.core import initialize_bot, activate_bot, retire_bot, return_possible_cs
from bots.models import Bot
from bots.tick import tick


class BotView(ListView):
    model = Bot
    queryset = Bot.objects.select_related('army')


@require_POST
def execute_tick(request):
    tick()

    return redirect('bots:bot-list')


@require_POST
def init(request):
    initialize_bot()

    return redirect('bots:bot-list')


@require_POST
def activate(request, bot_id: int):
    bot = get_object_or_404(Bot, id=bot_id)
    activate_bot(bot)

    return redirect('bots:bot-list')


@require_POST
def return_cs(request, bot_id: int):
    bot = get_object_or_404(Bot, id=bot_id)
    return_possible_cs(bot)

    return render(request, 'bots/bot_list.html', {'object_list': Bot.objects.select_related('army').all()})


@require_POST
def retire(request, bot_id: int):
    bot = get_object_or_404(Bot, id=bot_id)
    retire_bot(bot)

    return redirect('bots:bot-list')
