import logging

from django.conf import settings
from django.utils import timezone

from bots.core import initialize_bot, activate_bot, retire_bot, return_possible_cs
from bots.models import Bot

logger = logging.getLogger(__name__)


def tick() -> None:
    """
    Main cycle, brain of whole bot nation.

    This is what cronjob should invoke.

    It can create new bots, retire existing ones, create job alliance, return CS etc.
    """
    _check_bot_retiring()
    _check_activate_new_bot()


def _check_activate_new_bot():
    """
    Checks whether there is enough bots.

    If not, creates and activates new one.
    """
    active_bots = len(Bot.objects.filter(active=True))
    if active_bots >= settings.BOTS_MAX_COUNT:
        logger.debug('%d/%d bots active, no need to spin up a new one.', active_bots, settings.BOTS_MAX_COUNT)
        return

    logger.info("%d/%d bots active, creating a new one.", active_bots, settings.BOTS_MAX_COUNT)

    # is there a bot we can reuse?
    bot = Bot.objects.filter(active=False).order_by('updated').first()
    if bot is None:
        bot = initialize_bot()

    activate_bot(bot)


def _check_bot_retiring():
    """
    Checks whether there is a bot that is about to retire.

    Note that before retirement, you should return CS.
    """
    now = timezone.now()
    bot = Bot.objects.filter(active=True, expires_returns_at__lt=now).first()
    if bot is None:
        logger.debug("There is no bot retiring prior to %s", now)
        return

    attack_made = return_possible_cs(bot)

    if not attack_made:
        retire_bot(bot)
