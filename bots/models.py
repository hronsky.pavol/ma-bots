from django.db import models


class Army(models.Model):
    label = models.CharField(max_length=64)

    color = models.CharField(max_length=2)

    def __str__(self):
        return '[%s | %d]' % (self.label, self.id)


class ArmyUnit(models.Model):
    army = models.ForeignKey(
        Army,
        on_delete=models.CASCADE,
    )

    ma_unit_id = models.PositiveIntegerField()

    count_percent = models.FloatField(
        null=True,
        blank=True,
    )

    count_absolute = models.PositiveIntegerField(
        null=True,
        blank=True,
    )

    experience = models.FloatField()


class CombatSpellSettings(models.Model):
    army = models.ForeignKey(
        Army,
        on_delete=models.CASCADE,
    )

    tier = models.PositiveSmallIntegerField()

    ma_spell_1_id = models.PositiveIntegerField(
        blank=True,
        null=True,
    )

    ma_spell_2_id = models.PositiveIntegerField(
        blank=True,
        null=True,
    )

    ma_spell_3_id = models.PositiveIntegerField(
        blank=True,
        null=True,
    )

    ma_spell_4_id = models.PositiveIntegerField(
        blank=True,
        null=True,
    )

    def __str__(self):
        return '[tier %d | %d-%d-%d-%d | %d]' % (self.tier, self.ma_spell_1_id, self.ma_spell_2_id, self.ma_spell_3_id,
                                                 self.ma_spell_4_id, self.id)


class Bot(models.Model):
    name = models.CharField(max_length=255)
    province = models.CharField(max_length=255)
    created = models.DateTimeField(
        auto_now_add=True,
    )
    updated = models.DateTimeField(
        auto_now=True,
    )

    color = models.CharField(max_length=2)

    army = models.ForeignKey(
        Army,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    combat_spell_settings = models.ForeignKey(
        CombatSpellSettings,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    army_power = models.PositiveIntegerField(
        null=True,
        blank=True,
    )

    active = models.BooleanField()
    expires_returns_at = models.DateTimeField(
        null=True,
        blank=True
    )

    return_cs_safety_flag = models.PositiveIntegerField(
        default=0,
        help_text="Last CS time, used to make sure in case of cyclic error, each CS is tried just once.",
    )

    ma_province_id = models.PositiveIntegerField()
    ma_login = models.CharField(max_length=20)
    ma_password = models.CharField(max_length=20)

    def __str__(self):
        return '[%s | %s | %d]' % (self.name, self.province, self.id)


class UsersLogin(models.Model):
    undown = models.CharField(unique=True, max_length=20)
    heslo = models.CharField(max_length=20)
    ip = models.CharField(max_length=15)
    code = models.CharField(max_length=32)
    code2 = models.CharField(max_length=32)
    kontrola_ip = models.PositiveIntegerField()
    kontrola_cook = models.PositiveIntegerField()
    sledovat = models.PositiveIntegerField()
    browser = models.CharField(max_length=255)
    email = models.CharField(max_length=80)
    send_hlidka = models.IntegerField()
    hlidka_mail = models.CharField(max_length=80)
    reserved_spravce = models.CharField(max_length=50)
    reserved_provi = models.CharField(max_length=50)
    ver_text = models.CharField(max_length=8)
    verified = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'users_login'


class Users(models.Model):
    spravce = models.CharField(unique=True, max_length=255)
    provincie = models.CharField(unique=True, max_length=255)
    email = models.CharField(max_length=50, blank=True, null=True)
    pohlavi = models.CharField(max_length=1)
    slava = models.SmallIntegerField(default=0)
    slava_max = models.SmallIntegerField(default=0)
    titul = models.CharField(max_length=64)
    id_povolani = models.PositiveIntegerField()
    barva = models.CharField(max_length=1)
    presvedceni = models.CharField(max_length=1)
    puvodni_presvedceni = models.CharField(max_length=1)
    specializace = models.CharField(max_length=2)
    id_rasa = models.PositiveIntegerField()
    caszalozeni = models.DateTimeField()
    last_visit = models.DateField(auto_now_add=True)
    time_posl_tu = models.PositiveIntegerField(default=0)
    zbyva_tu = models.SmallIntegerField()
    power = models.PositiveIntegerField(default=0)
    zlato = models.IntegerField(default=0)
    mana = models.IntegerField(default=0)
    mana_max = models.IntegerField(default=0)
    lide = models.IntegerField()
    plocha = models.PositiveIntegerField()
    zbyva_plocha = models.PositiveIntegerField(default=0)
    odehrano_tahu = models.PositiveIntegerField()
    brana = models.CharField(max_length=32)
    brana_no = models.PositiveIntegerField(default=0)
    max_brana_no = models.IntegerField()
    dane = models.PositiveSmallIntegerField()
    protekce = models.PositiveSmallIntegerField()
    protekce_od_casu = models.IntegerField()
    posledni_utok = models.DateTimeField()
    protekce_ma = models.PositiveSmallIntegerField()
    sk = models.PositiveSmallIntegerField(default=0)
    id_aliance = models.IntegerField(default=0)
    account_id = models.IntegerField(default=0)
    verified = models.IntegerField()
    is_bot = models.IntegerField(default=0)

    class Meta:
        managed = False
        db_table = 'users'


class NocniKlid(models.Model):
    id_user = models.IntegerField(primary_key=True)
    start_time = models.TimeField(default='00:00:00')
    end_time = models.TimeField(default='00:00:00')
    next_change = models.DateTimeField(auto_now_add=True)
    posilat_hlidku_v_noci = models.IntegerField(default=0)

    class Meta:
        managed = False
        db_table = 'nocni_klid'


class UspesnostSouboju(models.Model):
    id_user = models.PositiveIntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'uspesnost_souboju'


class Bdv(models.Model):
    id_user = models.PositiveIntegerField()
    pocet = models.PositiveIntegerField()
    id_bdv = models.PositiveSmallIntegerField()

    class Meta:
        managed = False
        db_table = 'bdv'
        unique_together = (('id_user', 'id_bdv'),)


class Aliance(models.Model):
    id_aliance = models.IntegerField(primary_key=True)
    presvedceni = models.CharField(max_length=1)
    nazev_aliance = models.CharField(max_length=150)
    popis_aliance = models.CharField(max_length=255)
    vudce = models.PositiveIntegerField()
    zastupce = models.IntegerField(default=0)
    clenove = models.TextField()
    cekajici = models.TextField(default='')
    body = models.IntegerField(default=0)
    is_bot_alliance = models.IntegerField(default=0)

    class Meta:
        managed = False
        db_table = 'aliance'


class Ally(models.Model):
    id_user = models.PositiveIntegerField()
    id_aliance = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'ally'
        unique_together = (('id_user', 'id_aliance'),)


class MaUsersSetup(models.Model):
    user_id = models.PositiveIntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'ma_users_setup'


class MaUsersSpecials(models.Model):
    id_user = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'ma_users_specials'


class MaUsersSetupMenus(models.Model):
    user_id = models.PositiveIntegerField()
    item_id = models.PositiveIntegerField()
    menu_set = models.PositiveIntegerField()
    item_order = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'ma_users_setup_menus'
        unique_together = (('user_id', 'item_id'),)


class Jedn(models.Model):
    id_user = models.PositiveIntegerField()
    id_jednotky = models.PositiveSmallIntegerField()
    pocet_jedn = models.PositiveIntegerField()
    zkusenost = models.FloatField()

    class Meta:
        managed = False
        db_table = 'jedn'
        unique_together = (('id_user', 'id_jednotky'),)


class Jednotky(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    power = models.FloatField()

    class Meta:
        managed = False
        db_table = 'jednotky'


class Kouzla(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    nazev = models.CharField(max_length=150)
    barva = models.CharField(max_length=1)
    special = models.CharField(max_length=1)
    druh = models.CharField(max_length=3)
    mana_sesl = models.PositiveIntegerField()
    sesl_tu = models.PositiveSmallIntegerField()
    sesl_tu_posl = models.PositiveIntegerField()
    trvani_tu = models.PositiveSmallIntegerField()
    ucinek = models.TextField()
    popis = models.CharField(max_length=255)
    text_pri_seslani = models.CharField(max_length=255)
    cena_zl = models.PositiveIntegerField()
    cena_mn = models.PositiveIntegerField()
    vynalezani_tu = models.PositiveIntegerField()
    brana = models.PositiveIntegerField()
    skm = models.PositiveIntegerField(db_column='SKM')  # Field name made lowercase.
    skb = models.PositiveIntegerField(db_column='SKB')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kouzla'


class Kzl(models.Model):
    id_kzl = models.PositiveSmallIntegerField()
    id_user = models.PositiveIntegerField()
    ucinek = models.FloatField()

    class Meta:
        managed = False
        db_table = 'kzl'
        unique_together = (('id_user', 'id_kzl'),)


class Obrana(models.Model):
    id_user = models.PositiveIntegerField(primary_key=True)
    id_kzl_1 = models.SmallIntegerField(default=0)
    id_kzl_2 = models.SmallIntegerField(default=0)
    id_kzl_3 = models.SmallIntegerField(default=0)
    id_kzl_4 = models.SmallIntegerField(default=0)

    class Meta:
        managed = False
        db_table = 'obrana'


class Rezisty(models.Model):
    id_user = models.PositiveIntegerField()
    barva = models.CharField(max_length=2)
    prirozeny_r = models.PositiveIntegerField(default=0)
    zvyseny_r = models.PositiveIntegerField(default=0)
    do_tahu = models.PositiveIntegerField(default=0)

    class Meta:
        managed = False
        db_table = 'rezisty'
        unique_together = (('id_user', 'barva'),)


class Utoky(models.Model):
    kdo = models.PositiveIntegerField()
    koho = models.PositiveIntegerField()
    kdy = models.PositiveIntegerField()
    vyhral = models.PositiveIntegerField()
    protekce_pouzito = models.PositiveIntegerField()
    protiutok_pouzito = models.PositiveIntegerField()
    je_to_prvoutok = models.PositiveIntegerField()
    id_boje = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'utoky'
