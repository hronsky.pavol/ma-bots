import logging
from typing import Optional

import requests
from bs4 import BeautifulSoup
from django.conf import settings

from bots.models import Bot, UsersLogin

logger = logging.getLogger(__name__)


def refresh_prov_load_main(bot: Bot):
    users_login = UsersLogin.objects.get(id=bot.ma_province_id)

    requests.get('%s/main.html' % settings.BOTS_MA_BASE_URL,
                 headers={
                     'User-Agent': users_login.browser,
                 },
                 params={
                     'id': users_login.id,
                     'code': users_login.code,
                 },
                 cookies={
                     users_login.id: users_login.code2
                 })


def load_form_prepare_attack(bot: Bot, defender_id: int, battle_id: int) -> Optional[dict]:
    """
    Loads main <form> from a Prepare to attack page.

    If the form is not found for any reason, returns None
    """
    users_login = UsersLogin.objects.get(id=bot.ma_province_id)

    response = requests.get('%s/utok.html' % settings.BOTS_MA_BASE_URL,
                            headers={
                                'User-Agent': users_login.browser,
                                'Referer': settings.BOTS_MA_COOKIE_SERVER,
                            },
                            params={
                                'id': users_login.id,
                                'code': users_login.code,
                                'koho': defender_id,
                                'boj': battle_id,
                            },
                            cookies={
                                users_login.id: users_login.code2
                            })

    soup = BeautifulSoup(response.text, 'html.parser')
    form = soup.find('form', {'name': 'formular'})
    if form is None:
        logger.error('Cannot find a form in HTML for %s targeting %d (battle %d), cannot return CS',
                     bot, defender_id, defender_id)
        return None

    form_inputs = dict({e['name']: e.get('value', '') for e in form.find_all('input', {'name': True})})

    logger.info("Bot %s loaded his attack screen to %d (battle %d), found keys %s",
                bot, defender_id, battle_id, form_inputs)

    return form_inputs


def load_form_confirm_attack(bot: Bot, form_inputs: dict) -> Optional[dict]:
    """
    Loads main <form> from a Confirm attack page.

    If the form is not found for any reason, returns None
    """
    users_login = UsersLogin.objects.get(id=bot.ma_province_id)

    response = requests.post('%s/boj.html' % settings.BOTS_MA_BASE_URL,
                             headers={
                                 'User-Agent': users_login.browser,
                                 'Referer': settings.BOTS_MA_COOKIE_SERVER,
                             },
                             data=form_inputs,
                             cookies={
                                 users_login.id: users_login.code2
                             })

    soup = BeautifulSoup(response.text, 'html.parser')

    form = soup.find('form', {'action': 'bitva.html'})
    if form is None:
        logger.error('Cannot find an attack form in HTML for %s, data %s', bot, form_inputs)
        return None

    form_inputs = dict({e['name']: e.get('value', '') for e in form.find_all('input', {'name': True})})

    logger.info("Bot %s loaded his confirmation attack screen, found keys %s", bot, form_inputs)

    return form_inputs


def commence_attack(bot: Bot, form_inputs: dict) -> None:
    """
    Commence the invasion, comrade!
    """
    users_login = UsersLogin.objects.get(id=bot.ma_province_id)

    response = requests.post('%s/bitva.html' % settings.BOTS_MA_BASE_URL,
                             headers={
                                 'User-Agent': users_login.browser,
                                 'Referer': settings.BOTS_MA_COOKIE_SERVER,
                             },
                             data=form_inputs,
                             cookies={
                                 users_login.id: users_login.code2
                             })

    print(response.headers)
    print(response.text[:500])

    soup = BeautifulSoup(response.text, 'html.parser')

    combat_result = soup.find('span', class_='attack_result_det')
    if combat_result is None:
        logger.info("Bot %s finished his combat, result not found in HTML. Input %s", bot, form_inputs)
        return

    logger.info("Bot %s finished his combat, result %s. Input %s", bot, combat_result.text, form_inputs)
