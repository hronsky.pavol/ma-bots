import logging
from datetime import timedelta
from typing import Dict
from typing import List

from django.conf import settings
from django.db.models import Avg, Q
from django.utils import timezone

from bots.http import refresh_prov_load_main, load_form_prepare_attack, load_form_confirm_attack, commence_attack
from bots.models import ArmyUnit, Jedn, Kouzla, Kzl, Obrana, CombatSpellSettings, Rezisty, Aliance, Ally, Utoky
from bots.models import Bot, UsersLogin, Users, NocniKlid, UspesnostSouboju, Bdv, MaUsersSetup, MaUsersSpecials, \
    MaUsersSetupMenus, Army, Jednotky
from bots.random import rand_alnum, rand_email, rand_regent_sex, rand_class, rand_num, rand_chance, rand_float, \
    rand_int, rand_quote

logger = logging.getLogger(__name__)

CASTLE_BUILDING_ID = 5
BROWSER = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
BOT_ALLIANCE_ID = -42
BOT_MANA = 2_000_000


def initialize_bot() -> Bot:
    """
    Creates a new, blank, bot.

    Sets common province parameters, login/password, but no army.

    Bot is in `active=false` state.
    """
    logger.info("Initializing new bot...")

    (regent, sex) = rand_regent_sex()
    province = 'Planet %s' % regent
    login = password = rand_alnum()
    (class_id, color, spec, class_name) = rand_class()
    logger.info("He is born as %s (%s) from %s, class/color %s%s",
                regent, sex, province, color, spec)

    users_login = UsersLogin(
        undown=login,
        heslo=password,
        ip='A.D.F.G',
        code=rand_num(32),
        code2=rand_num(32),
        kontrola_ip=1,
        kontrola_cook=1,
        browser=BROWSER,
        email=rand_email(),
        reserved_spravce=regent,
        reserved_provi=province,
        ver_text=rand_alnum(8),
        verified=1,
        sledovat=0,
        send_hlidka=0,
    )
    users_login.save()
    user_id = users_login.id

    user = Users(
        id=user_id,
        spravce=regent,
        provincie=province,
        pohlavi=sex,
        # to disable attack prevention
        caszalozeni=timezone.now() - timedelta(hours=48),
        presvedceni='N',
        puvodni_presvedceni='N',
        id_povolani=class_id,
        id_rasa=2,
        protekce_od_casu=int(timezone.now().timestamp()),
        posledni_utok=timezone.now(),
        odehrano_tahu=300,
        protekce=1,
        protekce_ma=1,
        plocha=2000,
        zbyva_tu=360,
        zlato=100_000,
        lide=100_000,
        dane=70,
        max_brana_no=9,
        specializace=spec,
        # TODO shown in combat, recalculate
        titul='RoBoT',
        brana='Ex Aequo',
        barva=color,
        # allow quick login
        verified=1,
        is_bot=1,
    )
    user.save()

    NocniKlid(id_user=user_id).save()
    UspesnostSouboju(id_user=user_id).save()

    Bdv(id_user=user_id, id_bdv=CASTLE_BUILDING_ID, pocet=1).save()

    MaUsersSetup(user_id=user_id).save()
    MaUsersSpecials(id_user=user_id).save()

    bulk = []
    for (item_id, menu_set, item_order) in ((1, 1, 1),
                                            (2, 2, 2),
                                            (3, 2, 1),
                                            (4, 2, 2),
                                            (5, 1, 10),
                                            (6, 1, 5),
                                            (7, 2, 1),
                                            (8, 1, 10),
                                            (9, 1, 4),
                                            (10, 1, 10),
                                            (11, 1, 3),
                                            (12, 2, 1),
                                            (13, 2, 1),
                                            (14, 1, 2),
                                            (15, 1, 12),
                                            (16, 2, 12)):
        bulk.append(
            MaUsersSetupMenus(
                user_id=user_id,
                item_id=item_id,
                menu_set=menu_set,
                item_order=item_order
            )
        )
    MaUsersSetupMenus.objects.bulk_create(bulk)

    # Spells (knows all for his color)
    bulk = []
    for spell in Kouzla.objects.filter(barva__in=[color, '0'], druh='B'):
        bulk.append(
            Kzl(id_kzl=spell.id, id_user=user_id, ucinek=70.0)
        )
    Kzl.objects.bulk_create(bulk)

    Obrana(id_user=user_id).save()

    bot = Bot(
        name=regent,
        province=province,
        ma_province_id=user_id,
        ma_login=login,
        ma_password=password,
        color='%s%s' % (color, spec),
        active=False,
    )

    bot.save()

    _add_to_bot_alliance(bot, user)

    return bot


def activate_bot(bot: Bot) -> None:
    """
    Activates bot, calculates random power, area, sets when it should go inactive/return CS.
    """
    logger.info("Activating bot %s", bot)

    if bot.active:
        logger.error("Bot %s is already active!", bot)
        return

    average_area = _find_average_area()
    average_ot = _find_average_ot()
    bot_area = rand_int(int(average_area * 0.4), average_area)
    army_power = _army_power(bot_area)
    spell_tier, spell_xp = _spell_tier_xp_by_ot(average_ot)
    spell_resist = _spell_resist_by_ot(average_ot)

    logger.debug("Bot %s area: %d, army power: %d, spell tier %d, spell xp %.3f, spell resist %.3f",
                 bot, bot_area, army_power, spell_tier, spell_xp, spell_resist)

    user_id = bot.ma_province_id
    user = Users.objects.get(id=user_id)
    user.mana = BOT_MANA
    user.zlato = bot_area * 75
    user.lide = bot_area * 50
    user.plocha = bot_area
    user.zbyva_tu = 360
    user.slava = int(bot_area / 100)
    if user.slava > user.slava_max:
        user.slava_max = user.slava
    user.protekce_ma = 120
    user.odehrano_tahu = 2_600  # for 70% pacts
    user.protekce = 1

    user.save()

    _refresh_buildings(bot, bot_area)

    # Select army
    army = Army.objects.filter(color=bot.color).order_by('?').first()
    combat_spell_settings = CombatSpellSettings.objects.filter(army=army, tier=spell_tier).first()
    logger.info("Selected army for %s: %s", bot, army)
    logger.info("Selected spell settings for %s: %s", bot, combat_spell_settings)

    # MEGA_ARMY
    if rand_chance(settings.BOTS_MEGA_ARMY_CHANCE):
        logger.info("Bot %s has MEGA ARMY", bot)
        army = Army.objects.filter(color='XX').order_by('?').first()

    _update_army_to(bot, army, army_power)
    _update_defense_spells_to(bot, combat_spell_settings)

    # update spell xp
    Kzl.objects.filter(id_user=user_id).update(ucinek=spell_xp * 100.0)

    _update_resistance(bot, user.odehrano_tahu, spell_resist)

    # refresh provi in MA, recalculates full power
    refresh_prov_load_main(bot)

    # update bot flags
    bot.army = army
    bot.combat_spell_settings = combat_spell_settings
    bot.army_power = army_power
    bot.active = True
    bot.expires_returns_at = timezone.now() + timedelta(
        seconds=rand_int(settings.BOTS_RETURN_AFTER_SECONDS_MIN, settings.BOTS_RETURN_AFTER_SECONDS_MAX))
    bot.save()
    logger.info("Bot %s returns / expires at %s", bot, bot.expires_returns_at)

    # remove from prot
    user.protekce = 0
    user.save(update_fields=['protekce'])
    logger.info("Bot %s updated, removing safety 1TU prot", bot)


def retire_bot(bot: Bot) -> None:
    user = Users.objects.filter(id=bot.ma_province_id).first()
    if user is not None:
        user.protekce = 1
        user.save(update_fields=['protekce'])

    bot.active = False
    bot.save()
    logger.info("Sent bot %s to protection and deactivated him.", bot)


def return_possible_cs(bot: Bot) -> bool:
    """
    Tries to find a CS and if there is one, return it.

    Returns True iff an attack was attempted (even if the code then fails), or False if there are no more CS to try
    and province should retire to inactive.
    """
    attack = Utoky.objects.filter(
        Q(kdy__gt=(timezone.now() - timedelta(hours=48)).timestamp())
        &
        Q(kdy__gt=bot.return_cs_safety_flag),
        koho=bot.ma_province_id,
        je_to_prvoutok=1,
        protiutok_pouzito=0,
    ).values('kdo', 'kdy', 'id_boje').order_by('-kdy').first()

    if attack is None:
        logger.debug("There is no CS for %s", bot)
        return False

    logger.info("There is a CS for bot %s: target %d, since %d, battle_id %d",
                bot, attack['kdo'], attack['kdy'], attack['id_boje'])

    # before attack, reset mana and give protection (good also in case of exceptions, provi stays in prot 5 TU)
    Users.objects.filter(id=bot.ma_province_id).update(mana=BOT_MANA, protekce=5)
    bot.return_cs_safety_flag = attack['kdy']
    bot.save(update_fields=['return_cs_safety_flag'])

    army_power = rand_int(int(0.75 * bot.army_power), int(1.35 * bot.army_power))
    logger.debug("Army power for CS of bot %s: %d", bot, army_power)
    _update_army_to(bot, bot.army, army_power)

    prepare_form_inputs = load_form_prepare_attack(bot, attack['kdo'], attack['id_boje'])
    if prepare_form_inputs is None:
        logger.error('Problem returning CS for %s (%s)', bot, attack)
        return True

    prepare_form_inputs['kouzlo_1'] = bot.combat_spell_settings.ma_spell_1_id or 0
    prepare_form_inputs['kouzlo_2'] = bot.combat_spell_settings.ma_spell_2_id or 0
    prepare_form_inputs['kouzlo_3'] = bot.combat_spell_settings.ma_spell_3_id or 0
    prepare_form_inputs['kouzlo_4'] = bot.combat_spell_settings.ma_spell_4_id or 0
    prepare_form_inputs['kouzlo_1_koho'] = 1
    prepare_form_inputs['kouzlo_2_koho'] = 1
    prepare_form_inputs['kouzlo_3_koho'] = 1
    prepare_form_inputs['kouzlo_4_koho'] = 1

    confirm_form_inputs = load_form_confirm_attack(bot, prepare_form_inputs)
    if confirm_form_inputs is None:
        logger.error('Problem returning CS for %s (%s)', bot, attack)
        return True

    commence_attack(bot, confirm_form_inputs)

    # after CS, give him prot again, just to keep it clean and tidy
    Users.objects.filter(id=bot.ma_province_id).update(protekce=1)

    return True


def _add_to_bot_alliance(bot: Bot, user: Users) -> None:
    if user.id_aliance is not None and user.id_aliance > 0:
        # already in alliance
        return

    alliance = _get_or_create_bot_alliance(bot)

    user.id_aliance = alliance.id_aliance
    user.save(update_fields=['id_aliance'])

    Ally(
        id_user=bot.ma_province_id,
        id_aliance=alliance.id_aliance,
    ).save()

    alliance_user_key = ',%d,' % bot.ma_province_id
    if alliance_user_key in alliance.clenove:
        # alliance creator
        return

    alliance.clenove += alliance_user_key
    alliance.save()


def _get_or_create_bot_alliance(bot: Bot) -> Aliance:
    """
    Tries to lookup bot alliance.

    If not found, new is created.

    If found, as an easter egg, it's message is changed to a random DBZ quote ;)
    """
    alliance = Aliance.objects.filter(is_bot_alliance=1).first()

    if alliance is not None:
        alliance.popis_aliance = rand_quote()
        alliance.save(update_fields=['popis_aliance'])
        return alliance

    alliance = Aliance(
        id_aliance=BOT_ALLIANCE_ID,
        presvedceni='N',
        nazev_aliance='Z-Fighters',
        popis_aliance=rand_quote(),
        vudce=bot.ma_province_id,
        clenove=',%d,' % bot.ma_province_id,
        is_bot_alliance=1,
    )
    alliance.save()
    return alliance


def _spell_tier_xp_by_ot(ot: int) -> (int, float):
    if ot > 6000:
        return 2, rand_float(0.5, 0.8)

    if ot > 4000:
        return 2, rand_float(0.2, 0.5)

    if ot > 2000:
        return 1, rand_float(0.5, 0.8)

    return 1, rand_float(0.2, 0.5)


def _spell_resist_by_ot(ot: int) -> float:
    if ot > 8000:
        return 0.75

    if ot > 5000:
        return 0.75 * 0.95

    if ot > 3000:
        return 0.75 * 0.85

    if ot > 2000:
        return 0.75 * 0.75

    return 0.75 * 0.4


def _army_power(area: int) -> int:
    """
    Army power based on area.
    """
    return int(rand_float(7.0, 10.5) * area)


def _find_average_area() -> int:
    """
    Finds average area over TOP 12 players
    """
    users = list(user.plocha for user in Users.objects.filter(is_bot=False).order_by('-plocha')[:12])
    average_area = int(sum(users) / len(users))
    logger.debug("Average area of TOP12: %d", average_area)
    return average_area


def _find_average_ot() -> int:
    """
    Finds average TU played over players with OT > 500
    """
    average_ot = int(Users.objects.filter(odehrano_tahu__gt=500, is_bot=False)
                     .aggregate(Avg('odehrano_tahu'))['odehrano_tahu__avg'])
    logger.debug("Average area of players with 500+ OT: %d", average_ot)
    return average_ot


def _update_resistance(bot: Bot, user_ot: int, resist: float) -> None:
    Rezisty.objects.filter(id_user=bot.ma_province_id).delete()

    bulk = []
    for c in ('B', 'C', 'F', 'M', 'S', 'Z'):
        bulk.append(
            Rezisty(id_user=bot.ma_province_id,
                    barva=c,
                    zvyseny_r=int(resist * 100),
                    do_tahu=user_ot + 1_000)
        )

    Rezisty.objects.bulk_create(bulk)


def _refresh_buildings(bot: Bot, area: int) -> None:
    # demolish everything except castle
    Bdv.objects.filter(id_user=bot.ma_province_id).exclude(id_bdv=CASTLE_BUILDING_ID).delete()

    bulk = []
    # buildings
    for (building_id, count) in (
            (2, int(area * 0.1)),  # farms
            (6, int(area * 0.09)),  # mag towers
            (25, int(area * 0.045)),  # mag labs
            (1, int(area * 0.0054)),  # towns
            (3, int(area / 5000)),  # forts
            (10, 4_000),  # spies
    ):
        if count > 0:
            logger.debug("Built %dx%d for %s", count, building_id, bot)
            bulk.append(
                Bdv(id_user=bot.ma_province_id, id_bdv=building_id, pocet=count)
            )

    Bdv.objects.bulk_create(bulk)


def _update_army_to(bot: Bot, army: Army, army_power: int) -> None:
    logger.info("Updating army of %s to %s", bot, army)

    Jedn.objects.filter(id_user=bot.ma_province_id).delete()

    if army is None:
        return

    units_setup = army.armyunit_set.all()  # type: List[ArmyUnit]
    ma_units = Jednotky.objects.filter(id__in=[unit.ma_unit_id for unit in units_setup])

    ma_units_by_id = {unit.id: unit for unit in ma_units}  # type: Dict[int, Jednotky]

    bulk = []
    for unit_setup in units_setup:
        ma_unit = ma_units_by_id[unit_setup.ma_unit_id]

        if unit_setup.count_absolute is not None:
            count = unit_setup.count_absolute
        else:
            count = int(army_power
                        * unit_setup.count_percent
                        / unit_setup.experience
                        / ma_unit.power)

        if count < 1:
            count = 1

        bulk.append(Jedn(
            id_user=bot.ma_province_id,
            id_jednotky=ma_unit.id,
            pocet_jedn=count,
            zkusenost=unit_setup.experience * 100,
        ))

    Jedn.objects.bulk_create(bulk)


def _update_defense_spells_to(bot: Bot, combat_spell_settings: CombatSpellSettings) -> None:
    defense = Obrana.objects.get(id_user=bot.ma_province_id)
    defense.id_kzl_1 = 0
    defense.id_kzl_2 = 0
    defense.id_kzl_3 = 0
    defense.id_kzl_4 = 0

    if combat_spell_settings is not None:
        defense.id_kzl_1 = combat_spell_settings.ma_spell_1_id
        defense.id_kzl_2 = combat_spell_settings.ma_spell_2_id
        defense.id_kzl_3 = combat_spell_settings.ma_spell_3_id
        defense.id_kzl_4 = combat_spell_settings.ma_spell_4_id

    defense.save()
