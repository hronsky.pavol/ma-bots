from django.urls import path

from bots import views
from bots.views import BotView

app_name = 'bots'
urlpatterns = [
    path('', BotView.as_view(), name='bot-list'),
    path('tick/', views.execute_tick, name='tick'),
    path('init-bot/', views.init, name='init'),
    path('activate/<int:bot_id>/', views.activate, name='activate'),
    path('return-cs/<int:bot_id>/', views.return_cs, name='return_cs'),
    path('retire/<int:bot_id>/', views.retire, name='retire'),
]
